from collections import namedtuple

SimulationPlan = namedtuple('AirportConfig',
                            ['name', 'queue_type', 'revision_chance', 'revision_duration', 'station_count',
                             'start_time', 'end_time', 'boarding_duration', 'schedule_filename', 'log_filename',
                             'min_check_in_time', 'max_check_in_time'])
SimulationPlan.__doc__ = 'Config for single simulation instance'
