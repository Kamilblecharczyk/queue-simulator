import csv
import os
from collections import deque
from datetime import time, date, datetime, timedelta
from random import randint, random
import simulation_config as cfg

DEBUG = True


class Passenger(object):
    def __init__(self, name, min_time, max_time):
        """
        :param min_time: minimal time of handling passenger without including revision.
        :param max_time: maximum time of handling passenger without including revision.
        """
        self.name = name
        self.boarding_duration = randint(min_time, max_time)
        self.queued = False
        self.boarded = False
        self.flight = None  # Is assigned after creation.
        self.boarded_in_time = None

    def check_in(self, current_time):
        self.boarded = True
        self.boarded_in_time = current_time
        log("{} boarded at {}".format(self.__repr__(), current_time))

    def queue_in(self, current_time):
        self.queued = True
        log("{} queued at {}".format(self.__repr__(), current_time))

    def is_flight_open(self, current_time):
        return self.flight.is_boarding_open(current_time)

    def __repr__(self):
        return '<Passenger {}>'.format(self.name)


class Station(object):

    def __init__(self, name, queue_deque, revision_chance, revision_duration):
        """
        :param revision_chance: 0-1, chance that passenger will take additional revision_length to be checked_in
        :param revision_duration: Additional time in seconds.
        """
        self.name_id = name
        self.revision_chance = revision_chance
        self.revision_duration = revision_duration
        self.is_open = True
        self.queue = queue_deque
        self.passenger_checking_in = None
        self.finish_check_in_time = None

    def __repr__(self):
        return "<Station {}>".format(self.name_id)

    def _check_for_revision(self):
        return random() <= self.revision_chance

    def open(self):
        self.is_open = True

    def close(self):
        self.is_open = False

    def get_next_passenger(self, current_time):
        while self.passenger_checking_in is None or not self.passenger_checking_in.is_flight_open(self.finish_check_in_time):
            if len(self.queue) == 0:
                return
            self.passenger_checking_in = self.queue.popleft()
            self.set_next_finish_time(current_time)

    def set_next_finish_time(self, current_time):
        self.finish_check_in_time = current_time + timedelta(seconds=self.passenger_checking_in.boarding_duration)
        if self._check_for_revision():
            self.finish_check_in_time += timedelta(seconds=self.revision_duration)

    def check_in_passenger(self, current_time):
        if self.is_open:
            if self.passenger_checking_in is None:
                self.get_next_passenger(current_time)
            if self.finish_check_in_time is not None and self.finish_check_in_time <= current_time:
                self.passenger_checking_in.check_in(current_time)
                log("{} checked in passenger {} at {}".format(self.__repr__(), self.passenger_checking_in,
                                                              current_time))
                self.passenger_checking_in = None
                self.finish_check_in_time = None
        else:
            log("{} is closed".format(self.__repr__()))


class Flight(object):
    def __init__(self, passengers, destination, departure_time, boarding_duration):
        self.passengers = passengers
        for passenger in self.passengers:
            passenger.flight = self
        self.name = destination + ' at ' + departure_time
        self.departure_time = fake_datetime(time.fromisoformat(departure_time))
        self.boarding_duration = boarding_duration

    def is_boarding_open(self, current_time):
        return self.departure_time >= current_time >= self.departure_time - timedelta(
            seconds=self.boarding_duration)

    def are_passengers_boarded(self):
        return all([passenger.boarded for passenger in self.passengers])

    def are_passengers_queued(self):
        return all([passenger.queued for passenger in self.passengers])

    def last_boarding_time(self):
        return max([passenger.boarded_in_time for passenger in self.passengers if passenger.boarded_in_time], default=None)

    def __repr__(self):
        return '<Flight {}>'.format(self.name)


class Airport(object):
    def __init__(self, settings):
        self.settings = settings
        self.flights = []
        self.boarding_duration = settings.boarding_duration
        self.load_data(settings.schedule_filename)

        if settings.queue_type == 'single':
            self.queues = [deque()]
            self.stations = self.prepare_single_queue_stations()
        elif settings.queue_type == 'multiple':
            self.queues = [deque() for _ in range(settings.station_count)]
            self.stations = self.prepare_multiple_queue_stations()

        self.current_time = fake_datetime(time.fromisoformat(settings.start_time))
        self.close_time = fake_datetime(time.fromisoformat(settings.end_time))

    def load_data(self, filename):
        with open(os.getcwd() + '/data/' + filename, newline='') as f:
            csv_reader = csv.DictReader(f)
            for flight_row in csv_reader:
                self.flights.append(
                    Flight(
                        passengers=self.create_flight_passengers(flight_row['Seats']),
                        destination=flight_row['Destination'],
                        departure_time=flight_row['Time'],
                        boarding_duration=self.boarding_duration)
                )
        log("{} loaded flight data".format(self.__repr__()), 1)

    def create_flight_passengers(self, seats):
        return [
            Passenger(
                name=seat_no + 1,
                min_time=self.settings.min_check_in_time,
                max_time=self.settings.max_check_in_time
            ) for seat_no in range(int(seats))
        ]

    def prepare_single_queue_stations(self):
        return [
            Station(
                name=number + 1,
                queue_deque=self.queues[0],
                revision_chance=self.settings.revision_chance,
                revision_duration=self.settings.revision_duration)
            for number in range(self.settings.station_count)
        ]

    def prepare_multiple_queue_stations(self):
        return [
            Station(
                name=number + 1,
                queue_deque=self.queues[number],
                revision_chance=self.settings.revision_chance,
                revision_duration=self.settings.revision_duration)
            for number in range(self.settings.station_count)
        ]

    def simulate(self):
        log("Simulation begins for {}".format(self.__repr__()), 0)
        while self.current_time <= self.close_time:
            self.current_time += timedelta(seconds=1)
            self.add_flights_to_queue()
            for station in self.stations:
                station.check_in_passenger(self.current_time)
        self.write_results()
        log("Simulation ends for {}".format(self.__repr__()), 0)

    def write_results(self):
        with open('data/' + cfg.RESULT_FILENAME, 'a', newline='') as result_file:
            writer = csv.writer(result_file)
            writer.writerow(['Symulacja', 'Lot', 'Wszyscy na pokładzie', 'Ostatnia Obsługa'])
            for flight in self.flights:
                log("{} has successfully been boarded: {}".format(flight, flight.are_passengers_boarded()), 0)
                writer.writerow([self.settings.name, flight.name, flight.are_passengers_boarded(), flight.last_boarding_time()])

    def add_flights_to_queue(self):
        for flight in self.flights:
            if flight.is_boarding_open(self.current_time) and not flight.are_passengers_queued():
                self.queue_in_passengers(flight.passengers)

    def shortest_queue(self):
        return min(self.queues, key=len)

    def queue_in_passengers(self, passengers):
        for passenger in passengers:
            self.shortest_queue().append(passenger)
            passenger.queue_in(self.current_time)

    def __repr__(self):
        return '<{}>'.format(self.settings.name)


def fake_datetime(time):
    return datetime.combine(date.today(), time)


def log(text, level=5):
    if level <= cfg.PRINT_LEVEL:
        print(text)
    if level <= cfg.FILE_LEVEL:
        with open('data/' + cfg.TARGET_FILENAME, 'a') as f:
            f.write(text + '\n')
