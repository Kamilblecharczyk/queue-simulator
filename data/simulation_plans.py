from plans import SimulationPlan

PLANS = [  # Currently contains example plans
    SimulationPlan(
        name='Lotnisko 1, pojedyncza kolejka',
        queue_type='single',
        revision_chance=0.2,
        revision_duration=5,
        station_count=8,
        start_time='05:45',
        end_time='17:45',
        boarding_duration=3600,
        schedule_filename='test_data.csv',
        log_filename='single.log',
        min_check_in_time=1,
        max_check_in_time=1
    ),
    SimulationPlan(
        name='Lotnisko 2, wiele kolejek',
        queue_type='multiple',
        revision_chance=0.2,
        revision_duration=5,
        station_count=8,
        start_time='05:45',
        end_time='17:45',
        boarding_duration=3600,
        schedule_filename='test_data.csv',
        log_filename='first.log',
        min_check_in_time=1,
        max_check_in_time=1
    ),
]