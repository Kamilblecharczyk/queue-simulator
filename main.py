#!/usr/bin/env python
from simulation import Airport
from data.simulation_plans import PLANS
import argparse

parser = argparse.ArgumentParser(description="Run a queue simulation")

if __name__ == '__main__':
    args = parser.parse_args()
    for plan in PLANS:
        simulated_airport = Airport(plan)
        simulated_airport.simulate()
