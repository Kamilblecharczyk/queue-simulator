# Logging
PRINT_LEVEL = 1  # Highest level of logs to be printed in console. Use -1 for no printing.
FILE_LEVEL = 5  # Highest level of logs to be printed to file. Use -1 for no printing
TARGET_FILENAME = 'simulation.log'  # Filename to which you want to save logs

FLIGHT_SCHEDULE = 'flights.csv'  # CSV file containing scheduled flights

RESULT_FILENAME = 'results.csv'
