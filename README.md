### What is this?
A quick students projects for university course.  

### Instalation
First clone this repository. Then create Env with Conda:
```
conda create --name <name_of_env> --file requirements.txt
```

### How to use
Running only this file will show you help
```commandline
$ python main.py -h
```

Example usage:

```commandline
$    python main.py 100 5 single 30 45 0.2 15
```
Will run simulation for 100 passengers with 5 stations served by single queue.
Each passenger will be served for 30-45 seconds, with 20% chance of 15 seconds extra spend
on revision.

